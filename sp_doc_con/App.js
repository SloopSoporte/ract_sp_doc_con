/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  PermissionsAndroid,
} from "react-native";
import firebase from "react-native-firebase";
import { Navigation } from "react-native-navigation";
import MapScreen from "./screens/map";
import Login from "./screens/login";
import Mapbox from "@mapbox/react-native-mapbox-gl";

Mapbox.setAccessToken(
  "pk.eyJ1Ijoic2xvb3AiLCJhIjoiY2p0ZzdzeHlkMG5reDQzbnU4MnJ3ZmQ0cCJ9.5ZM8lyWZYegGPCSLBRq5CQ"
);


//type Props = {};
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  /**
   * When the App component mounts, we listen for any authentication
   * state changes in Firebase.
   * Once subscribed, the 'user' parameter will either be null
   * (logged out) or an Object (logged in)
   */
  componentDidMount() {
    Platform.OS === "android"
      ? PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
          ],
          {
            title: "Give Location Permission",
            message: "App needs location permission to find your position."
          }
        )
          .then(granted => {
            console.log(granted);
            resolve();
          })
          .catch(err => {
            console.warn(err);
          })
      : null;
    this.authSubscription = firebase.auth().onAuthStateChanged(user => {
      this.setState({
        loading: false,
        user
      });
      if(user){
        global.uid = user.uid;
      }
     
      
    });
    
  }

  /**
   * Don't forget to stop listening for authentication state changes
   * when the component unmounts.
   */
  componentWillUnmount() {
    this.authSubscription();
  }

  render() {
    if (this.state.loading)
      return <ActivityIndicator size="large" color="#0000ff" />;

    if (this.state.user) {
      Navigation.setRoot({
        root: {
          sideMenu: {
            //id: "sideMenu",
            left: {
              component: {
                id: "Drawer",
                name: "navigation.menu"
              }
            },
            center: {
              stack: {
                id: "appstack",
                children: [
                  {
                    component: {
                      name: "navigation.map"
                    }
                  }
                ],
                options: {
                  topBar: {
                    background: {
                      color: "#0F3277"
                    },
                    visible: false,
                    drawBehind:true
                  }
                }
              },
              
            },
          }
        }
      });
      return <MapScreen />;
    } else {
      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: "navigation.login"
                }
              }
            ],
            options: {
              statusBar: {
                backgroundColor: "#0B2659"
              },

              topBar: {
                /*title: {
                  text: 'Iniciar Sesion',
                  color: '#fff',
                  alignment: 'center'
                },*/
                background: {
                  color: "#0F3277"
                }
              }
            }
          }
        }
      });
      return <Login />;
    }

    // The user is null, so they're logged out
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
