import React, { Component } from 'react';
import { Text, StyleSheet, View, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import Mapbox from "@mapbox/react-native-mapbox-gl";
import StoreLocatorKit from '@mapbox/store-locator-react-native';
import { Content, Button } from 'native-base';
import Geocode from "react-geocode";
import { Navigation } from "react-native-navigation";

Geocode.setApiKey("AIzaSyCEEwQWl0s359H8fzSoKyfrpBlQj270jxc");



export default class ride extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            email: "",
            nombre: "",
            paterno: "",
            materno: "",
            telefono: "",
            password: "",
            newPassword: "",
            confirmPassword: "",
            address: null,
            rideId: this.props.rideId
        };
        this.usersRef = firebase
            .firestore()
            .collection("users")
            .doc("UggfLWjXerbEfqHe3JeRdTBgKdj1");
    }

    async componentDidMount() {
        this._isMounted = true;
        this.getUserInfo()

        Geocode.fromLatLng(this.props.ridelat, this.props.ridelng).then(
            response => {
                const address = response.results[0].formatted_address;
                this.setState({
                    address: address
                })
            },
            error => {
                console.error(error);
            }
        );

        this.getRides();
    }

    async getRides() {
        await firebase
            .firestore()
            .collection("rides")
            .doc(this.state.rideId)
            .onSnapshot(function(doc) {
               if(doc.data().rideStatus != "0"){
                   Alert.alert("El viaje ya no se encuentra disponible");
               }
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    getUserInfo() {
        this.usersRef.onSnapshot(doc => {
            if (!doc.exists) {
                console.log("No such document!");
            } else {
                if (this._isMounted) {
                    this.setState({
                        nombre: doc.data().Nombre,
                        paterno: doc.data().paterno,
                        driverStatus: doc.data().status
                    });
                }
                console.log("Document data:", doc.data());
            }
        });
    }

    accept = (id) => {
        var db = firebase.firestore().collection("rides").doc(id);
        return db.update({
            rideStatus: 1,
            driverId: global.uid
        })
            .then(function () {
                console.log("Document successfully updated!");
            })
            .catch(function (error) {
                console.error("Error updating document: ", error);
            });
    }

    goToMap(){
        Navigation.popToRoot(this.props.componentId);
    }


    render() {
        return (
            // {/* <Text style={{ fontSize: 20, fontWeight: 'bold', alignSelf: 'center', marginTop: 5 }}>Informacion</Text> */ }

            <View style={{ flex: 1 }}>
                <Text style={{ alignSelf: 'center', marginTop: 10, marginBottom: 5, fontSize: 20, fontWeight: 'bold' }}>Ubicacion del usuario</Text>
                <Mapbox.MapView
                    ref={c => {
                        this.mapref = c;
                    }}
                    styleURL={"mapbox://styles/sloop/cjb1pm13frsxv2snw4oh4rxwa"}
                    centerCoordinate={[-100.360418, 25.6537428]}
                    style={styles.container}
                    // showUserLocation={true}
                    userTrackingMode={Mapbox.UserTrackingModes.FollowWithCourse}
                    // rotateEnabled={true}
                    logoEnabled={true}
                    //onUserLocationUpdate={this.centerMap}
                    //onDidFinishLoadingMap={this.centerMap}
                    zoom={0}
                    // pitch={30}
                    heading={0}
                    duration={800}
                />

                <View style={{ flex: 1, backgroundColor: "white" }}>
                    <Text style={{ alignSelf: 'center', marginTop: 10, marginBottom: 5, fontSize: 20, fontWeight: 'bold' }}>Direccíon</Text>
                    <Text style={{ marginHorizontal: 10 }}>{this.state.address}</Text>
                    <Text style={{ alignSelf: 'center', marginTop: 10, marginBottom: 5, fontSize: 20, fontWeight: 'bold' }}>Distancia</Text>
                    <Text style={{ alignSelf: 'center', marginTop: 5, fontSize: 15 }}>se encuentra a: {this.props.rideDistance} Km</Text>
                    <Button style={{ margin: 10 }} block bordered success onPress={() => this.accept(this.props.rideId)}>
                        <Text>Aceptar</Text>
                    </Button>
                    <Button style={{ margin: 10 }} onPress={() => this.goToMap()} block bordered danger>
                        <Text>Volver</Text>
                    </Button>
                    <Text>{this.props.rideId.toString()}</Text>
                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    actionButtonIcon: {
        color: "white"
    },
    container: {
        flex: 1
    }
});