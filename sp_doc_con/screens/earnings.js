import React, { Component } from "react";
import { Text, TouchableOpacity, StyleSheet, View } from "react-native";
import { Content, Icon, Card, CardItem, Right, Left } from "native-base";

export default class earnings extends Component {
  render() {
    return (
      <Content>
        <Text
          style={{
            alignSelf: "center",
            fontSize: 25,
            color: "green",
            marginTop: 10
          }}
        >
          Ingresos por día
        </Text>
        <View
          style={{
            marginBottom: 20,
            marginTop: 25,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            onPress={this.showMenu}
            style={{
              borderWidth: 1,
              borderColor: "rgba(0,0,0,0.2)",
              alignItems: "center",
              justifyContent: "center",
              marginRight: 10,
              width: 30,
              height: 30,
              backgroundColor: "#fff",
              borderRadius: 100
            }}
          >
            <Icon
              name="navigate-before"
              type="MaterialIcons"
              style={{ fontSize: 30, color: "green" }}
            />
          </TouchableOpacity>

          <Text style={{ fontSize: 25, color: "green" }}>$ 800.00</Text>

          <TouchableOpacity
            onPress={this.showMenu}
            style={{
              borderWidth: 1,
              borderColor: "rgba(0,0,0,0.2)",
              alignItems: "center",
              justifyContent: "center",
              marginLeft: 10,
              width: 30,
              height: 30,
              backgroundColor: "#fff",
              borderRadius: 100
            }}
          >
            <Icon
              name="navigate-next"
              type="MaterialIcons"
              style={{ fontSize: 30, color: "green" }}
            />
          </TouchableOpacity>
        </View>
        <Text
          style={{
            alignSelf: "center",
            fontSize: 25,
            color: "green",
            marginTop: 10
          }}
        >
          24/02/2020
        </Text>
        <Card style={{marginTop: 20}}>
          <CardItem>
            <Icon active name="logo-googleplus" />
            <Text>Google Plus</Text>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </CardItem>
          <CardItem>
            <Icon active name="logo-googleplus" />
            <Text>Google Plus</Text>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </CardItem>
          <CardItem>
            <Icon active name="logo-googleplus" />
            <Text>Google Plus</Text>
            <Right>
              <Icon name="arrow-forward" />
            </Right>
          </CardItem>
        </Card>
      </Content>
    );
  }
}

const styles = StyleSheet.flatten({});
