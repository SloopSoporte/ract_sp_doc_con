import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  Image,
  ScrollView,
  Platform
} from "react-native";
import { Navigation } from "react-native-navigation";
import {
  Content,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Container,
  Footer,
  FooterTab,
  Header
} from "native-base";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent,
  ScaleAnimation,
  DialogTitle
} from "react-native-popup-dialog";
import firebase from  'react-native-firebase';

const uri = "https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png";

export default class profile extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      email: "",
      nombre: "",
      paterno: "",
      materno: "",
      telefono: "",
      password: "",
      newPassword: "",
      confirmPassword: ""
    };
    this.usersRef = firebase
      .firestore()
      .collection("doctors")
      .doc(global.uid);
  }

  getUserInfo() {
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            nombre: doc.data().nombre,
            paterno: doc.data().paterno,
            driverStatus: doc.data().status
          });
        }
        console.log("Document data:", doc.data());
      }
    });
  }

  componentDidMount(){
    this._isMounted = true;
    this.getUserInfo();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <Content>
        {Platform.OS === "android" ? <Text style={{marginTop: 20}}/> : null}
        <View style={styles.avatarContainer}>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Image style={styles.avatar} source={{ uri }} />
            <Text>Foto de frente</Text>
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Image style={styles.avatar} source={{ uri }} />
            <Text>Foto de perfil</Text>
          </View>
        </View>
        <Button
          block
          primary
          bordered
          style={{
            margin: 10
          }}
          onPress={() => this.setState({ visible: true })}
        >
          <Text style={{ color: "#212121" }}>Cambiar contraseña</Text>
        </Button>
        <Form style={{ marginRight: 5 }}>
          <Item stackedLabel>
            <Label>Nombre</Label>
            <Input disabled value={this.state.nombre} />
          </Item>
          <Item stackedLabel>
            <Label>Apellido paterno</Label>
            <Input disabled value={this.state.paterno}/>
          </Item>
          <Item stackedLabel>
            <Label>Apellido materno</Label>
            <Input disabled value={this.state.materno}/>
          </Item>
          <Item stackedLabel>
            <Label>Correo electrónico</Label>
            <Input disabled value={this.state.email}/>
          </Item>
          <Item stackedLabel>
            <Label>Telefono celular</Label>
            <Input value={this.state.telefono}/>
          </Item>
        </Form>

        <Button
          block
          success
          style={{
            margin: 10,
            marginBottom: 50
          }}
        >
          <Text style={{ color: "#fff" }}>Guardar cambios</Text>
        </Button>

        {/*cambiar contraseña*/}
        <Dialog
          onTouchOutside={() => {
            this.setState({ visible: false });
          }}
          width={0.9}
          visible={this.state.visible}
          dialogAnimation={new ScaleAnimation()}
          dialogTitle={
            <DialogTitle
              title="Cambiar contraseña"
              hasTitleBar={false}
              marginTop="10"
            />
          }
          footer={
            <DialogFooter>
              <Text />
              <DialogButton
                text="Aceptar"
                onPress={() => {
                  this.setState({ visible: false });
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Item stackedLabel last style={{ marginBottom: 10 }}>
              <Label>Contraseña anterior</Label>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                returnKeyType="done"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
            <Item stackedLabel last style={{ marginBottom: 10 }}>
              <Label>Contraseña nueva</Label>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                returnKeyType="done"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
            <Item stackedLabel last style={{ marginBottom: 10 }}>
              <Label>Confirmar contraseña</Label>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                returnKeyType="done"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
              />
            </Item>
          </DialogContent>
        </Dialog>
      </Content>
    );
  }
}

const styles = StyleSheet.flatten({
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    margin: 20
  }
});
