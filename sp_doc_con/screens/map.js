import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Platform,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  Text
} from "react-native";
import { Icon } from "native-base";
import ActionButton from "react-native-action-button";
import firebase from "react-native-firebase";
import { Navigation } from "react-native-navigation";
import Mapbox from "@mapbox/react-native-mapbox-gl";
import { SkypeIndicator } from "react-native-indicators";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent,
  ScaleAnimation,
  DialogTitle
} from "react-native-popup-dialog";
import geolib from "geolib";

export default class map extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.state = {
      map_style: "mapbox://styles/sloop/cjb1pm13frsxv2snw4oh4rxwa",
      ready: false,
      where: { lat: null, lng: null },
      error: null,
      popInfo: null,
      visible: false,
      nottitle: null,
      notbody: null,
      ride: false,
      ridelat: null,
      ridelng: null,
      rideId: null,
      rideDistance: null,
      refused: [],
      aux: false
    };
  }

  async componentDidMount() {
    this._isMounted = true;
    let geoOptions = {
      enableHighAccuaracy: true,
      timeOut: 20000,
      maximumAge: 60 * 60 * 24
    };
    //posicion actual
    navigator.geolocation.getCurrentPosition(
      this.geoSuccess,
      this.geoFailure,
      geoOptions
    );
    //seguir el movimiento de mi ubicacion
    this.watchId = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          where: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          },
          ready: true,
          error: null
        });
      },
      error => Alert.alert(error.message),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 60 * 60 * 24,
        distanceFilter: 10
      }
    );
    this.checkPermission();
    this.createNotificationListeners();
    this.getRides(); //add this line
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    this.notificationListener();
    this.notificationOpenedListener();
    this._isMounted = false;
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    let fcmToken = await AsyncStorage.getItem("fcmToken");
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem("fcmToken", fcmToken);
      }
    }
    //this.setState({visible: true, nottitle: "mi token", notbody: fcmToken});
    firebase
      .firestore()
      .collection("doctors")
      .doc(global.uid)
      .update({
        uniqueToken: fcmToken
      });
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
    }
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body } = notification;
        //this.showAlert(title, body);
        this.setState({ nottitle: title, notbody: body, visible: true });
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen.notification;
        this.showAlert(title, body);
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  async getRides() {
    await firebase
      .firestore()
      .collection("rides")
      .onSnapshot(querySnapshot => {
        querySnapshot.docs.forEach(ride => {
          if (
            this.state.ready &&
            this.state.ride == false &&
            ride.data().rideStatus == 0 &&
            ride.data().lat != null
          ) {
            //limpiar auxiliar para la lista de viajes rechazados
            this.setState({
              aux: false
            });
            //calcula la distancia entre la solicitud y tu ubicacion
            var distance = geolib.getDistance(
              { latitude: ride.data().lat, longitude: ride.data().lng },
              {
                latitude: this.state.where.lat,
                longitude: this.state.where.lng
              }
            );
            // condicional de distancia
            if (distance < 5000) {
              this.compareRefused(ride.id);
              if (this.state.aux == false) {
                this.setState({
                  ride: true,
                  ridelat: ride.data().lat,
                  ridelng: ride.data().lng,
                  rideDistance: distance,
                  rideId: ride.id
                });
              }
            }
          }
        });
      });
  }

  async compareRefused(rideId) {
    await this.state.refused.map(item => {
      if (item.ride == rideId) {
        //auxiliar para saber si el id de viaje ya esta en lista de rechazados
        this.setState({
          aux: true
        });
      }
    });
  }

  refuse = () => {
    // Create a new array based on current state:
    let refused = [...this.state.refused];

    // Add item to it
    refused.push({ ride: this.state.rideId });

    // Set state
    this.setState({ refused, ride: false });
  };

  accept(){
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: "navigation.ride",
        passProps: {
          rideId: this.state.rideId,
          rideDistance: this.state.rideDistance / 1000,
          ridelat: this.state.ridelat,
          ridelng: this.state.ridelng
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: "Detalles del servicio",
              color: "#fff"
            },
            background: {
              color: "#0F3277"
            },
            visible: true
          }
        }
      }
    });
  

  }

  showAlert(title, body) {
    Alert.alert(
      title,
      body,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  }

  geoSuccess = position => {
    //cambia el estado cuando ya esta montado el componente
    if (this._isMounted) {
      this.setState({
        ready: true,
        where: { lat: position.coords.latitude, lng: position.coords.longitude }
      });
    }
  };

  geoFailure = err => {
    if (this._isMounted) {
      this.setState({ error: err.message });
      Alert.alert(err.message);
    }
  };

  showMenu() {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  }

  centerMap = () => {
    this.locationref.reset();
    this.mapref.setCamera({
      centerCoordinate: [this.state.where.lng, this.state.where.lat],
      zoom: 17,
      pitch: 45,
      heading: 0,
      duration: 100,
      mode: Mapbox.CameraModes.Ease
    });
    firebase
      .firestore()
      .collection("doctors")
      .doc(global.uid)
      .update({
        latitude: this.state.where.lat,
        longitude: this.state.where.lng
      });
  };

  render() {
    if (!this.state.ready) {
      return <SkypeIndicator color="#0F3277" size={100} />;
    } else {
      return (
        <View style={styles.container}>
          {this.state.ready && (
            <Mapbox.MapView
              ref={c => {
                this.mapref = c;
              }}
              styleURL={this.state.map_style}
              //centerCoordinate={[this.state.where.lng, this.state.where.lat]}
              style={styles.container}
              showUserLocation={true}
              userTrackingMode={Mapbox.UserTrackingModes.FollowWithCourse}
              rotateEnabled={true}
              logoEnabled={true}
              onUserLocationUpdate={this.centerMap}
              onDidFinishLoadingMap={this.centerMap}
              zoom={17}
              pitch={45}
              heading={0}
              duration={100}
            />
          )}
          {/* popup viaje */}
          <Dialog
            onTouchOutside={() => {
              this.setState({ ride: false, error: null });
            }}
            width={0.9}
            visible={this.state.ride}
            dialogAnimation={new ScaleAnimation()}
            dialogTitle={
              <DialogTitle
                title={"Se ha solicitado un servicio"}
                hasTitleBar={false}
                marginTop="10"
              />
            }
            footer={
              <DialogFooter>
                <DialogButton
                  text="Rechazar"
                  onPress={() => {
                    this.refuse();
                  }}
                />
                <DialogButton
                  text="Mostrar detalles"
                  onPress={() => {
                    this.accept();
                  }}
                />
              </DialogFooter>
            }
          >
            <DialogContent>
              <Text style={{ alignSelf: "center" }}>
                distancia: {this.state.rideDistance / 1000}km
              </Text>
            </DialogContent>
          </Dialog>
          {/* popup notificaciones */}
          <Dialog
            onTouchOutside={() => {
              this.setState({ visible: false, error: null });
            }}
            width={0.9}
            visible={this.state.visible}
            dialogAnimation={new ScaleAnimation()}
            dialogTitle={
              <DialogTitle
                title={this.state.nottitle}
                hasTitleBar={false}
                marginTop="10"
              />
            }
            footer={
              <DialogFooter>
                <Text />
                <DialogButton
                  text="Aceptar"
                  onPress={() => {
                    this.setState({ visible: false });
                  }}
                />
              </DialogFooter>
            }
          >
            <DialogContent>
              <Text style={{ alignSelf: "center" }}>{this.state.notbody}</Text>
            </DialogContent>
          </Dialog>
          {/*boton de menu*/}
          <TouchableOpacity
            onPress={this.showMenu}
            style={{
              borderWidth: 1,
              borderColor: "rgba(0,0,0,0.2)",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              marginTop: 60,
              marginLeft: 10,
              flex: 1,
              width: 50,
              height: 50,
              backgroundColor: "#0F3277",
              borderRadius: 100
            }}
          >
            <Icon
              ios="ios-menu"
              android="md-menu"
              style={{ fontSize: 30, color: "#fff" }}
            />
          </TouchableOpacity>

          {/* boton para estilo de mapa */}
          <ActionButton
            buttonColor="rgba(231,76,60,1)"
            renderIcon={active =>
              active ? (
                <Icon name="options" style={styles.actionButtonIcon} />
              ) : (
                <Icon name="md-options" style={styles.actionButtonIcon} />
              )
            }
            verticalOrientation="up"
            offsetY={100}
            style={{ marginBottom: 20 }}
          >
            >
            <ActionButton.Item
              buttonColor="#FABC01"
              title="Modo día"
              onPress={() => {
                this.setState({
                  map_style: "mapbox://styles/sloop/cjb1pm13frsxv2snw4oh4rxwa"
                });
              }}
            >
              <Icon name="sun" type="FontAwesome5" style={{ color: "black" }} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="black"
              title="Modo noche"
              onPress={() => {
                this.setState({ map_style: Mapbox.StyleURL.Dark });
              }}
            >
              <Icon name="moon" style={{ color: "white" }} />
            </ActionButton.Item>
          </ActionButton>

          <ActionButton
            buttonColor="rgba(255,255,255,1)"
            ref={c => {
              this.locationref = c;
            }}
            renderIcon={active =>
              active ? (
                <Icon
                  name="my-location"
                  type="MaterialIcons"
                  style={{ fontSize: 30, color: "black" }}
                />
              ) : (
                <Icon
                  name="my-location"
                  type="MaterialIcons"
                  style={{ fontSize: 29, color: "black" }}
                />
              )
            }
            onPress={this.centerMap}
            style={{ marginBottom: 20 }}
          >
            >
          </ActionButton>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    color: "white"
  },
  container: {
    flex: 1
  }
});
