import React, { Component } from "react";
import { Container, Header, Content, Button, Icon, Text } from "native-base";
import { Dimensions, StyleSheet, ScrollView, View, Image } from "react-native";
import { Navigation } from "react-native-navigation";
import firebase from "react-native-firebase";
import { Switch } from "react-native-base-switch";

const window = Dimensions.get("window");
const uri = "https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png";

export default class Menu extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      nombre: "prueba",
      driverStatus: "En linea",
      switchStatus: true,
      paterno: ""
    };
   
  }

  componentDidMount() {
    global.uid = firebase.auth().currentUser.uid;
    this.usersRef = firebase
    .firestore()
    .collection("doctors")
    .doc(global.uid);
    this._isMounted = true;
    this.getUserInfo();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  saveUserInfo() {
    firebase
      .auth()
      .createUserWithEmailAndPassword(
        "pruebaPalEsteban@gmail.com",
        "SiFunciona"
      )
      .then(function success(userData) {
        var uid = userData.uid; // The UID of recently created user on firebase

        this.usersRef = firebase
          .firestore()
          .collection("users")
          .doc(uid);

        this.usersRef.set({
          email: "dfssdfdfs",

          someotherproperty: "some user preference"
        });
      })
      .catch(function failure(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode + " " + errorMessage);
      });
  }

  changeOnlineStatus = () => {
    if (this.state.driverStatus == "En linea") {
      this.usersRef.update({
        status: "Desconectado"
      });
      this.setState({
        switchStatus: false
      })
    } else {
      this.usersRef.update({
        status: "En linea"
      });
      this.setState({
        switchStatus: true
      })
    }
  };

  // getUserInfo() {
  //   this.usersRef
  //     .get()
  //     .then(doc => {
  //       if (!doc.exists) {
  //         console.log("No such document!");
  //       } else {
  //         if (this._isMounted) {
  //           this.setState({
  //             nombre: doc.data().nombre,
  //             paterno: doc.data().paterno,
  //             driverStatus: doc.data().status
  //           });
  //         }
  //         console.log("Document data:", doc.data());
  //       }
  //     })
  //     .catch(err => {
  //       console.log("Error getting document", err);
  //     });
  // }

  getUserInfo() {
    this.usersRef.onSnapshot(doc => {
      if (!doc.exists) {
        console.log("No such document!");
      } else {
        if (this._isMounted) {
          this.setState({
            nombre: doc.data().nombre,
            paterno: doc.data().paterno,
            driverStatus: doc.data().status
          });
        }
        console.log("Document data:", doc.data());
      }
    });
  }

  goToPage(Name, TopBarTittle) {
    Navigation.mergeOptions("Drawer", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    Navigation.push("appstack", {
      component: {
        name: Name,
        passProps: {
          text: "Pushed screen"
        },
        options: {
          topBar: {
            backButton: {
              color: "#fff"
            },
            title: {
              text: TopBarTittle,
              color: "#fff"
            },
            background: {
              color: "#0F3277"
            },
            visible: true
          }
        }
      }
    });
  }

  signOutUser = async () => {
    try {
      firebase
        .auth()
        .signOut()
        .then(user => {
          Navigation.setRoot({
            root: {
              stack: {
                children: [
                  {
                    component: {
                      name: "navigation.login"
                    }
                  }
                ],
                options: {
                  statusBar: {
                    backgroundColor: "#0B2659"
                  },

                  topBar: {
                    /*title: {
                      text: 'Iniciar Sesion',
                      color: '#fff',
                      alignment: 'center'
                    },*/
                    background: {
                      color: "#0F3277"
                    }
                  }
                }
              }
            }
          });
        });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <View style={styles.menu}>
        <ScrollView scrollsToTop={false} style={styles.menu}>
          <View style={styles.avatarContainer}>
            <Image style={styles.avatar} source={{ uri }} />
            <Text style={styles.name}>
              {this.state.nombre} {this.state.paterno}
            </Text>
          </View>
          <View
            style={{ flexDirection: "row", marginBottom: 25, marginLeft: 5 }}
          >
            <Switch
              inactiveButtonColor="#952332"
              inactiveButtonPressedColor="#952332"
              activeButtonColor="#83E2B1"
              activeButtonPressedColor="#83E2B1"
              active={this.state.switchStatus}
              style={{ marginTop: 10 }}
              onActivate={this.changeOnlineStatus}
              onDeactivate={this.changeOnlineStatus}
            />
            <Text style={{ color: "#fff", marginLeft: 20 }}>
              {this.state.driverStatus}
            </Text>
          </View>

          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.profile", "Mi perfil")}
          >
            <Icon name="person" type="MaterialIcons" />
            <Text>Mi perfil</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.history", "History")}
          >
            <Icon name="profile" type="AntDesign" />
            <Text>Historial</Text>
          </Button>
          <Button
            iconLeft
            transparent
            light
            onPress={() => this.goToPage("navigation.earnings", "Mis ingresos")}
          >
            <Icon name="attach-money" type="MaterialIcons" />
            <Text>Ingresos</Text>
          </Button>
        </ScrollView>
        <Button
          iconLeft
          transparent
          light
          bordered
          onPress={this.signOutUser}
          style={{
            position: "absolute",
            marginLeft: 40,
            marginBottom: 40,
            bottom: 10
          }}
        >
          <Icon name="logout" type="SimpleLineIcons" />
          <Text>Cerrar sesión</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    width: window.width,
    height: window.height,
    backgroundColor: "#0F3277",
    padding: 20
  },
  avatarContainer: {
    marginBottom: 20,
    marginTop: 20
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    flex: 1
  },
  name: {
    position: "absolute",
    left: 70,
    top: 20,
    color: "#fff"
  },
  item: {
    fontSize: 18,
    fontWeight: "300",
    paddingTop: 5,
    color: "#fff"
  }
});
