import {
  AppRegistry,
  AsyncStorage,
  View,
  ToolbarAndroid,
  ActivityIndicator
} from "react-native";
import {
  Header,
  Container,
  Form,
  Title,
  Item,
  Label,
  Content,
  List,
  ListItem,
  InputGroup,
  Input,
  Icon,
  Text,
  Picker,
  Button
} from "native-base";
import React, { Component } from "react";
import firebase from "react-native-firebase";
import ValidationComponent from "react-native-form-validator";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent,
  ScaleAnimation,
  DialogTitle
} from "react-native-popup-dialog";
import MapScreen from "./map";
import { Navigation } from "react-native-navigation";

Navigation.registerComponent(`navigation.map`, () => MapScreen);
export default class login extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      visible: false,
      error: ""
    };
  }

  onLogin = () => {
    this.validate({
      //name: {minlength:3, maxlength:7, required: true},
      email: { required: true },
      password: { required: true }
      //date: {date: 'YYYY-MM-DD'}
    });
    if (this.isFormValid()) {
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(user => {
          Navigation.setRoot({
            root: {
              sideMenu: {
                //id: "sideMenu",
                left: {
                  component: {
                    id: "Drawer",
                    name: "navigation.menu"
                  }
                },
                center: {
                  stack: {
                    id: "appstack",
                    children: [
                      {
                        component: {
                          name: "navigation.map"
                        }
                      }
                    ],
                    options: {
                      topBar: {
                        /*leftButtons: [
                          {
                            id: 'buttonOne',
                            icon: require('./icons/ic_menu.png'),
                            component: {
                              name: 'example.CustomButtonComponent'
                            },
                            text: 'Button one',
                            systemItem: 'done', // iOS only. Sets a system bar button item as the icon. Matches UIBarButtonSystemItem naming. See below for details.
                            enabled: false,
                            disableIconTint: false,
                            color: 'red',
                            disabledColor: 'black',
                            testID: 'buttonOneTestID'
                          }
                        ],
                        rightButtons: [
                          {
                            id: 'button two',
                            text: 'save',
                            color: 'red'
                          }
                        ],*/

                        /*title: {
                          text: 'Iniciar Sesion',
                          color: '#fff',
                          alignment: 'center'
                        },*/
                        background: {
                          color: "#0F3277"
                        },
                        visible: false,
                        drawBehind: true
                      }
                    }
                  }
                }
              }
            }
          });
          return <MapScreen />;
        })
        .catch(error => {
          switch (error.code) {
            /*case "emailAlreadyInUse":
              this.setState({
                error: "Este correo ya está siendo usado por otro usuario",
                visible: true
              });
              return;*/
            case "auth/user-disabled":
              this.setState({
                error: "Este usuario ha sido deshabilitado",
                visible: true
              });
              return;
            case "auth/operation-not-allowed":
              this.setState({ error: "Operación no permitida", visible: true });
              return;
            case "auth/invalid-email":
              this.setState({
                error: "Correo electrónico no valido",
                visible: true
              });
              return;
            case "auth/wrong-password":
              this.setState({
                error: "Contraseña incorrecta",
                visible: true
              });
              return;
            case "auth/user-not-found":
              this.setState({
                error:
                  "No se encontró cuenta del usuario con el correo especificado",
                visible: true
              });
              return;
            case "auth/network-request-failed":
              this.setState({
                error: "Problema al intentar conectar al servidor",
                visible: true
              });
              return;
            case "auth/invalid-password":
              this.setState({
                error: "Contraseña muy debil o no válida",
                visible: true
              });
              return;
            /*case "missingEmail":
              return "Hace falta registrar un correo electrónico";
            case "internalError":
              return "Error interno";
            case "invalidCustomToken":
              return "Token personalizado invalido";*/
            case "auth/too-many-requests":
              this.setState({
                error: "Ya se han enviado muchas solicitudes al servidor",
                visible: true
              });
              return;
          }
        });
    } else {
      this.setState({
        visible: true,
        error: "Por favor, rellene todos los campos"
      });
    }
  };

  saveUserInfo() {
    firebase
      .auth()
      .createUserWithEmailAndPassword("prueba8@gmail.com", "pruebaxD")
      .then(() => {
        //var uid = userData.uid; // The UID of recently created user on firebase
        var aux = firebase.auth().currentUser;
        firebase
          .firestore()
          .collection("users")
          .doc(aux.uid)
          .set({
            email: "dfssdfdfs",

            someotherproperty: "some user preference"
          });
      })
      .catch(function failure(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode + " " + errorMessage);
      });
  }

  render() {
    return (
      <Container style={{ justifyContent: "center" }}>
        <Form style={{ margin: 10 }}>
          <Item floatingLabel style={{ marginBottom: 10 }}>
            <Label>Correo</Label>
            <Input
              ref="email"
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="next"
              keyboardType="email-address"
              onChangeText={email => this.setState({ email })}
            />
          </Item>
          <Item floatingLabel last style={{ marginBottom: 10 }}>
            <Label>Contraseña</Label>
            <Input
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="done"
              secureTextEntry={true}
              onChangeText={password => this.setState({ password })}
            />
          </Item>
          <Button
            block
            success
            onPress={this.onLogin}
            style={{ marginTop: 10 }}
          >
            <Text>Iniciar sesión</Text>
          </Button>
        </Form>
        <Dialog
          onTouchOutside={() => {
            this.setState({ visible: false, error: null });
          }}
          width={0.9}
          visible={this.state.visible}
          dialogAnimation={new ScaleAnimation()}
          dialogTitle={
            <DialogTitle title="Error" hasTitleBar={false} marginTop="10" />
          }
          footer={
            <DialogFooter>
              <Text />
              <DialogButton
                text="Aceptar"
                onPress={() => {
                  this.setState({ visible: false });
                }}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <Text style={{ alignSelf: "center" }}>{this.state.error}</Text>
          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}
