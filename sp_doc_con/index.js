/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { Navigation } from "react-native-navigation";
import App from './App';
import MenuScreen from "./screens/menu";
import MapScreen from "./screens/map";
import LoginScreen from "./screens/login";
import ProfileScreen from "./screens/profile";
import HistoryScreen from "./screens/history";
import EarningsScreen from "./screens/earnings";
import RideScreen from "./screens/ride"

Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
Navigation.registerComponent(`navigation.login`, () => LoginScreen);
Navigation.registerComponent(`navigation.menu`, () => MenuScreen);
Navigation.registerComponent(`navigation.map`, () => MapScreen);
Navigation.registerComponent(`navigation.profile`, () => ProfileScreen);
Navigation.registerComponent(`navigation.history`, () => HistoryScreen);
Navigation.registerComponent(`navigation.earnings`, () => EarningsScreen);
Navigation.registerComponent(`navigation.ride`, () => RideScreen);


Navigation.events().registerAppLaunchedListener(() => {

  Navigation.setDefaultOptions({
    statusBar: {
      backgroundColor: '#0B2659',
      style: 'light'
    }
  });
  
  Navigation.setRoot({
    root: {
      component: {
        name: "navigation.playground.WelcomeScreen"
      }
    }
  });
})

